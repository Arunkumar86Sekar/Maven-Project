package MavenSpikeProject;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;


public class BaseDriver {
	public static final String USERNAME = "arunkumars4";
	public static final String AUTOMATE_KEY = "pLzuqaZQucpg7wsnysvE";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	@Test
	public static void meth() throws MalformedURLException {
		// System.setProperty("webdriver.gecko.driver","C:\\Users\\Cst_kumarax\\workspace\\MavenSpikeProject\\geckodriver-v0.10.0-win64\\geckodriver.exe");
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browser", "IE");
		caps.setCapability("browser_version", "7.0");
		caps.setCapability("os", "Windows");
		caps.setCapability("os_version", "XP");
		caps.setCapability("browserstack.debug", "true");

		WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
		driver.get("http://www.pluggedin.com/");

		/*
		 * WebElement element =
		 * driver.findElement(By.xpath(".//*[@id='mainmenu']/li[1]/a"));
		 * 
		 * element.click();
		 */

		System.out.println(driver.getTitle());
		driver.quit();

	}

}
